import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { EmployeeData, CommonService } from '../common.service';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {

  employeeDataForm: FormGroup | any;

  employeeData = new EmployeeData();

  constructor(private formBuilder: FormBuilder,private common:CommonService,private router:Router) { }

  ngOnInit(): void {
    this.employeeData = this.common.employeeData;
    this.employeeDataForm = this.formBuilder.group({
      empName: new FormControl('',),
      empContact: new FormControl('', ),
      empEmail: new FormControl('', ),
      empAddress: new FormControl('', ),
    })
  }
}
