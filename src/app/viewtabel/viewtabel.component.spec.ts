import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewtabelComponent } from './viewtabel.component';

describe('ViewtabelComponent', () => {
  let component: ViewtabelComponent;
  let fixture: ComponentFixture<ViewtabelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ViewtabelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewtabelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
