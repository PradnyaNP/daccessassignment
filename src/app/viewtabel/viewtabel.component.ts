import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonService, EmployeeData } from '../common.service';

@Component({
  selector: 'app-viewtabel',
  templateUrl: './viewtabel.component.html',
  styleUrls: ['./viewtabel.component.scss']
})
export class ViewtabelComponent implements OnInit {

  displayedColumns: string[] = ['srNo', 'empName', 'empAddress', 'empContact', 'empEmail', 'action'];
  dataSource: MatTableDataSource<EmployeeData> | any;

  employeeData = new EmployeeData();
  progVal:any;


  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort) sort: MatSort | any;

  constructor(private toastr: ToastrService,private router:Router,private common:CommonService) { }

  ngOnInit(): void {
    if(this.common.temp){
      this.employeeData.empName = "Pradnya Pawar";
      this.employeeData.empAddress = "Pune";
      this.employeeData.empContact = "9096563936";
      this.employeeData.empEmail = "pawarpradnya0203@gmail.com";
      this.common.employeeDataList.push(this.employeeData);

      this.dataSource = new MatTableDataSource(this.common.employeeDataList);
      this.progVal = this.common.employeeDataList.length*10;
      this.common.temp=false;
    }else{
      this.dataSource = new MatTableDataSource(this.common.employeeDataList);
      this.progVal = this.common.employeeDataList.length*10;
    }
  }

  save(){
    this.common.employeeData = new EmployeeData();
    this.router.navigateByUrl("/create");
  }

  delete(i:any){
    this.common.employeeDataList.splice(i,1);
    this.dataSource = new MatTableDataSource(this.common.employeeDataList);
    this.progVal = this.common.employeeDataList.length*10;
    this.toastr.success('Delete Data!', 'Success!');
  }

  edit(i:any){
    this.common.employeeData = this.common.employeeDataList[i];
    this.common.employeeDataList.splice(i,1);
    this.dataSource = new MatTableDataSource(this.common.employeeDataList);
    this.progVal = this.common.employeeDataList.length*10;
    this.router.navigateByUrl("/update");
  }

  view(i:number){
    this.common.employeeData = this.common.employeeDataList[i];
    this.router.navigateByUrl("/view");
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
