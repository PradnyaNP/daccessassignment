import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CreateComponent } from './create/create.component';
import { UpdateComponent } from './update/update.component';
import { ViewComponent } from './view/view.component';
import { ViewtabelComponent } from './viewtabel/viewtabel.component';

const routes: Routes = [
  { path: "", redirectTo: "/viewtabel", pathMatch: "full" },
      {
        path: "viewtabel",
        component: ViewtabelComponent,
      },
      {
        path: "view",
        component: ViewComponent,
      },
      {
        path: "create",
        component: CreateComponent,
      },
      {
        path: "update",
        component: UpdateComponent,
      },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
