import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class EmployeeData {
  id: any;
  empName: any;
  empAddress: any;
  empContact: any;
  empEmail:any;
}



export class CommonService {

  employeeDataList:Array<EmployeeData>=[];  

  temp:boolean=true;

  employeeData = new EmployeeData();

  constructor() { }
}
