import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { CommonService, EmployeeData } from '../common.service';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {

  employeeDataForm: FormGroup | any;

  employeeData = new EmployeeData();

  constructor(private formBuilder: FormBuilder,private common:CommonService,private toastr: ToastrService,private router:Router) { }

  ngOnInit(): void {

    this.employeeData = this.common.employeeData;

    this.employeeDataForm = this.formBuilder.group({
      empName: new FormControl('',[Validators.required]),
      empContact: new FormControl('', [Validators.required]),
      empEmail: new FormControl('', [Validators.required]),
      empAddress: new FormControl('', [Validators.required]),

    })

  }

  save(employeeData:EmployeeData){
    if(this.common.employeeDataList.length<10){
      this.common.employeeDataList.push(this.employeeData);
      this.employeeData = new EmployeeData();
      this.toastr.success('Save Data!', 'Success!');
      this.router.navigateByUrl("/viewtabel")
    }else{
      this.toastr.warning('Data full!', 'Warning!');
      this.clear();
      this.router.navigateByUrl("/viewtabel")
    }
  }

  clear(){
    this.employeeData = new EmployeeData();
    this.employeeDataForm.reset();
  }

  cancel(){
    this.router.navigateByUrl("/viewtabel")
  }
}